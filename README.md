# ShrooPHP\PSR

A library for building PSR-compliant applications with ShrooPHP.

## Installation

```
composer require 'shroophp/psr ^1.0'
```

## Example Usage

```php
<?php

use GuzzleHttp\Psr7\ServerRequest;
use ShrooPHP\Core\Request;
use ShrooPHP\Framework\Application;
use ShrooPHP\Framework\Request\Responses\Response;
use ShrooPHP\PSR\RequestHandler;

require 'vendor/autoload.php';

$default = new \GuzzleHttp\Psr7\Response(Response::HTTP_NOT_FOUND);
$handler = new RequestHandler($default);

$handler->apply(function (Application $app) {

	$app->path('/greeting', $app->_(function (Request $request) {

		return Response::string('Hello, world!', 'text/plain');
	}));
});

$no = $handler->handle(new ServerRequest('GET', '/'));         // 404 Not Found
$ok = $handler->handle(new ServerRequest('GET', '/greeting')); // 200 OK

````

## Important Notes

When applying an application modifier to the request handler, it is important
to specify each response indirectly as the return value of a callback (as
opposed to having the response as an explicit application modifier or handler).

This is because direct modifiers and handlers are not given the opportunity to
make use of the presenter associated with the application. In terms of the
request handler, this means that the response cannot be buffered and hence
converted into a PSR-compliant instance.
