<?php

namespace ShrooPHP\PSR\Http\Message;

use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Stream;
use Psr\Http\Message\ResponseInterface;
use ShrooPHP\Core\Request\Response as IResponse;
use ShrooPHP\Core\Runnable;
use ShrooPHP\Core\Bufferer as IBufferer;
use ShrooPHP\Core\Bufferers\Bufferer;
use ShrooPHP\PSR\Http\Message\ResponseTrait;

/**
 * An adapter that allows responses to be PSR-compliant.
 */
class ResponseAdapter implements ResponseInterface, Runnable
{
	use ResponseTrait;

	/**
	 * The default status code.
	 */
	const STATUS = 200;

	/**
	 * The default HTTP version.
	 */
	const VERSION = '1.1';

	/**
	 * The adapted response.
	 *
	 * @var \Psr\Http\Message\ResponseInterface
	 */
	private $response;

	/**
	 * The content associated with the response (if any).
	 *
	 * @var \ShrooPHP\Core\Runnable
	 */
	private $content;

	/**
	 * The bufferer being used to buffer the content of the response.
	 *
	 * @var \ShrooPHP\Core\Bufferer
	 */
	private $bufferer;

	/**
	 * Converts the given headers to a PSR-compliant format.
	 *
	 * @param \Traversable|array $headers the headers to convert
	 *
	 * @return array the converted headers
	 */
	public static function toHeaders($headers)
	{
		$converted = array();

		foreach ($headers as $header => $value) {

			if (!isset($converted[$header])) {
				$converted[$header] = array();
			}

			$converted[$header][] = $value;
		}

		return $converted;
	}

	/**
	 * Constructs an adapter for the given response.
	 *
	 * @param \ShrooPHP\Core\Request\Response $response the response to adapt
	 * @param string|null $version the HTTP version to associate with the
	 * response (or NULL to use the default)
	 * @param string|null $reason the HTTP reason phrase to associate with the
	 * response (or NULL to use the default)
	 * @param \ShrooPHP\Core\Bufferer|null $bufferer the bufferer to use in
	 * order to buffer the content of the response (or NULL to use the default)
	 *
	 */
	public function __construct(
			IResponse $response,
			$version = null,
			$reason = null,
			IBufferer $bufferer = null
	) {

		$status = $response->code();

		if (is_null($status)) {
			$status = self::STATUS;
		}

		if (is_null($version)) {
			$version = self::VERSION;
		}

		if (is_null($bufferer)) {
			$bufferer = new Bufferer;
		}

		$this->bufferer = $bufferer;
		$this->content = $response->content();
		$this->response = new Response(
				$status,
				self::toHeaders($response->headers()),
				null,
				$version,
				$reason
		);
	}

	public function getBody()
	{
		return new Stream($this->bufferer->buffer($this));
	}

	public function run()
	{
		if (!is_null($this->content)) {
			$this->content->run();
		}
	}

	protected function response()
	{
		return $this->response;
	}
}
