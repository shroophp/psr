<?php

namespace ShrooPHP\PSR\Http\Message;

use Psr\Http\Message\StreamInterface;

/**
 * Wrapper methods for classes implementing \Psr\Http\Message\ResponseInterface.
 */
trait ResponseTrait
{
	/**
	 * @see \Psr\Http\Message\ResponseInterface::getBody()
	 */
	public function getBody()
	{
		return $this->response()->getBody();
	}

	/**
	 * @see \Psr\Http\Message\ResponseInterface::getHeader()
	 */
	public function getHeader($name)
	{
		return $this->response()->getHeader($name);
	}

	/**
	 * @see \Psr\Http\Message\ResponseInterface::getHeaderLine()
	 */
	public function getHeaderLine($name)
	{
		return $this->response()->getHeaderLine($name);
	}

	/**
	 * @see \Psr\Http\Message\ResponseInterface::getHeaders()
	 */
	public function getHeaders()
	{
		return $this->response()->getHeaders();
	}

	/**
	 * @see \Psr\Http\Message\ResponseInterface::getProtocolVersion()
	 */
	public function getProtocolVersion()
	{
		return $this->response()->getProtocolVersion();
	}

	/**
	 * @see \Psr\Http\Message\ResponseInterface::getReasonPhrase()
	 */
	public function getReasonPhrase()
	{
		return $this->response()->getReasonPhrase();
	}

	/**
	 * @see \Psr\Http\Message\ResponseInterface::getStatusCode()
	 */
	public function getStatusCode()
	{
		return $this->response()->getStatusCode();
	}

	/**
	 * @see \Psr\Http\Message\ResponseInterface::hasHeader()
	 */
	public function hasHeader($name)
	{
		return $this->response()->hasHeader($name);
	}

	/**
	 * @see \Psr\Http\Message\ResponseInterface::withAddedHeader()
	 */
	public function withAddedHeader($name, $value)
	{
		return $this->response()->withAddedHeader($name, $value);
	}

	/**
	 * @see \Psr\Http\Message\ResponseInterface::withBody()
	 */
	public function withBody(StreamInterface $body)
	{
		return $this->response()->withBody($body);
	}

	/**
	 * @see \Psr\Http\Message\ResponseInterface::withHeader()
	 */
	public function withHeader($name, $value)
	{
		return $this->response()->withHeader($name, $value);
	}

	/**
	 * @see \Psr\Http\Message\ResponseInterface::withProtocolVersion()
	 */
	public function withProtocolVersion($version)
	{
		return $this->response()->withProtocolVersion($version);
	}

	/**
	 * @see \Psr\Http\Message\ResponseInterface::withStatus()
	 */
	public function withStatus($code, $reasonPhrase = '')
	{
		return $this->response()->withStatus($code, $reasonPhrase);
	}

	/**
	 * @see \Psr\Http\Message\ResponseInterface::withoutHeader()
	 */
	public function withoutHeader($name)
	{
		return $this->response()->withoutHeader($name);
	}

	/**
	 * Gets the response.
	 *
	 * @return \Psr\Http\Message\ResponseInterface the response
	 */
	protected abstract function response();

}
