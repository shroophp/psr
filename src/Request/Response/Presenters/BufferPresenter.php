<?php

namespace ShrooPHP\PSR\Request\Response\Presenters;

use ShrooPHP\Core\Request\Response;
use ShrooPHP\Core\Request\Response\Presenter;

/**
 * A presenter that buffers the mostly recently passed response.
 */
class BufferPresenter implements Presenter
{
	/**
	 * The response that was most recently passed to the presenter (if any).
	 *
	 * @var \ShrooPHP\Core\Request\Response|null
	 */
	private $response;

	public function present(Response $response)
	{
		$this->response = $response;
	}

	/**
	 * Gets the response that was most recently passed to the presenter (if
	 * any).
	 *
	 * @return \ShrooPHP\Core\Request\Response|null the response that was most
	 * recently passed to the presenter (if any)
	 */
	public function response()
	{
		return $this->response;
	}

	/**
	 * Clears the buffer of the response that was most recently passed to the
	 * presenter (if any).
	 */
	public function clear()
	{
		$this->response = null;
	}
}
