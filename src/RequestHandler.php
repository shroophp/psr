<?php

namespace ShrooPHP\PSR;

use GuzzleHttp\Psr7\ServerRequest;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use ShrooPHP\Core\Pattern\Interpreter;
use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Response as IResponse;
use ShrooPHP\Core\Session;
use ShrooPHP\Core\Token\Generator;
use ShrooPHP\Core\Request\Response\Presenter as IPresenter;
use ShrooPHP\Framework\Application;
use ShrooPHP\Framework\Application\Modifier;
use ShrooPHP\Framework\Application\Modifiers\CallbackAdapter;
use ShrooPHP\Framework\Request\Response\Presenters\Presenter;
use ShrooPHP\Framework\Request\Responses\Response;
use ShrooPHP\Framework\Requests\PhpRequest;
use ShrooPHP\PSR\Request\Response\Presenters\BufferPresenter;
use ShrooPHP\PSR\Http\Message\ResponseAdapter;
use ShrooPHP\PSR\Requests\ServerRequestAdapter;
use ShrooPHP\PSR\Requests\ServerRequestAdapter\Converter;
use SplObjectStorage;

/**
 * A PSR-compliant request handler that uses a pre-configured application.
 */
class RequestHandler implements RequestHandlerInterface
{
	/**
	 * The default response.
	 *
	 * @var \Psr\Http\Message\ResponseInterface
	 */
	private $response;

	/**
	 * The application being used to handle requests.
	 *
	 * @var \ShrooPHP\Framework\Application
	 */
	private $app;

	/**
	 * The presenter being used to buffer responses.
	 *
	 * @var \ShrooPHP\PSR\Request\Response\Presenters\BufferPresenter
	 */
	private $buffer;

	/**
	 * The session being associated with requests.
	 *
	 * @var \ShrooPHP\Core\Session
	 */
	private $session;

	/**
	 * The converter being used to convert headers.
	 *
	 * @var \ShrooPHP\PSR\Requests\ServerRequestAdapter\Converter
	 */
	private $converter;

	/**
	 * The currently known PSR-compliant instances.
	 *
	 * @var \SplObjectStorage
	 */
	private $requests;

	/**
	 * Constructs a PSR-compliant request handler that uses a pre-configured
	 * application.
	 *
	 * @param \Psr\Http\Message\ResponseInterface $response the default response
	 * @param \ShrooPHP\Core\Pattern\Interpreter|null $interpreter the
	 * interpreter to use in order to interpret patterns (or NULL to use the
	 * default)
	 * @param \ShrooPHP\Core\Token\Generator|null $generator the generator to
	 * use in order to generate tokens (or NULL to use the default)
	 * @param \ShrooPHP\Core\Session|null $session the session to associate
	 * with requests (or NULL to use the default)
	 * @param \ShrooPHP\PSR\Requests\ServerRequestAdapter\Converter|null
	 * $converter the converter to use in order to convert headers (or NULL to
	 * use the default)
	 */
	public function __construct(
			ResponseInterface $response,
			Interpreter $interpreter = null,
			Generator $generator = null,
			Session $session = null,
			Converter $converter = null
	) {

		$this->buffer = new BufferPresenter;
		$this->app = new Application($interpreter, $this->buffer, $generator);
		$this->response = $response;
		$this->session = $session;
		$this->converter = $converter;
		$this->requests = new SplObjectStorage;
	}

	/**
	 * Applies the given modifier to the underlying application.
	 *
	 * @param \ShrooPHP\Framework\Application\Modifier|callable $modifier the
	 * modifier to apply to the application
	 */
	public function apply($modifier)
	{
		if (!($modifier instanceof Modifier)) {
			$modifier = new CallbackAdapter($modifier);
		}

		$modifier->modify($this->app);
	}

	public function handle(ServerRequestInterface $request): ResponseInterface
	{
		$response = null;
		$converted = $this->toRequest($request);

		$this->buffer->clear();

		$this->requests->offsetSet($converted, $request);
		$handled = $this->app->handle($converted);
		$this->requests->offsetUnset($converted);

		if ($handled) {
			$response = $this->buffer->response();
		}

		return is_null($response)
				? $this->response
				: new ResponseAdapter($response);
	}

	/**
	 * Responds to the given request.
	 *
	 * @param \ShrooPHP\Core\Request $request the request to respond to
	 * @return \ShrooPHP\Core\Request\Response the response to the request
	 */
	public function respond(Request $request): IResponse
	{
		$response = $this->__invoke($request);

		if (is_null($response)) {
			$response = $this->toResponse($this->response);
		}

		return $response;
	}

	/**
	 * Responds to the given request.
	 *
	 * @param \ShrooPHP\Core\Request $request the request to respond to
	 * @return \ShrooPHP\Core\Request\Response|null the response to the request
	 * (if any)
	 */
	public function __invoke(Request $request)
	{
		$presenter = new BufferPresenter;
		$this->present($presenter, function () use ($request) {
			$this->app->handle($request);
		});

		return $presenter->response();
	}

	/**
	 * Extracts the current request from the server API and presents the
	 * response accordingly.
	 *
	 * @param \ShrooPHP\Core\Request\Response\Presenter|null $presenter the
	 * to use in order to present the response (or NULL to use the default)
	 * @see \ShrooPHP\Framework\Application::run()
	 */
	public function serve(IPresenter $presenter = null)
	{
		if (is_null($presenter)) {
			$presenter = new Presenter;
		}

		$presented = $this->present($presenter, function () {
			$request = PhpRequest::create();
			$converted = ServerRequest::fromGlobals();
			$this->requests->offsetSet($request, $converted);
			$this->app->handle($request);
			$this->requests->offsetUnset($request);
		});

		if (!$presented) {
			$presenter->present($this->toResponse($this->response));
		}
	}

	/**
	 * Converts the given request to the known PSR-compliant instance.
	 *
	 * @param \ShrooPHP\Core\Request $request the request to convert
	 * @return \Psr\Http\Message\ServerRequestInterface|null the converted
	 * request (or NULL if the PSR-compliant instance is unknown)
	 */
	public function toServerRequest(Request $request)
	{
		$converted = null;

		if ($this->requests->offsetExists($request)) {
			$converted = $this->requests->offsetGet($request);
		}

		return $converted;
	}

	/**
	 * Presents the response associated with the presenter after triggering
	 * the given callback.
	 *
	 * @param \ShrooPHP\Core\Request\Response\Presenter $presenter the
	 * presenter to use in order to present the response
	 * @param callable $callback the callback to trigger
	 * @return bool Whether or not a response was presented
	 */
	private function present(IPresenter $presenter, callable $callback): bool
	{
		$this->buffer->clear();
		$callback();

		$presented = false;
		$response = $this->buffer->response();

		if (!is_null($response)) {
			$presenter->present($response);
			$presented = true;
		}

		return $presented;
	}

	/**
	 * Converts the given server request to an application-compatible request.
	 *
	 * @param \Psr\Http\Message\ServerRequestInterface $request the request to
	 * convert
	 * @return \ShrooPHP\Core\Request the converted server request
	 */
	private function toRequest(ServerRequestInterface $request)
	{
		return new ServerRequestAdapter(
				$request,
				$this->session,
				$this->converter
		);
	}

	/**
	 * Converts the given PSR-compliant response.
	 *
	 * @param \Psr\Http\Message\ResponseInterface $response the response to
	 * convert
	 * @return \ShrooPHP\Core\Request\Response $response the converted
	 * response
	 */
	private function toResponse(ResponseInterface $response): Response
	{
		$body = $response->getBody();
		$converted = Response::callback(function () use ($body) {
			while ($body->isReadable() && !$body->eof()) {
				echo $body->read(0x1000);
			}
		});

		$converted->setCode($response->getStatusCode());

		foreach ($response->getHeaders() as $header => $values) {

			foreach ($values as $value) {

				$converted->addHeader($header, $value);
			}
		}

		return $converted;
	}
}
