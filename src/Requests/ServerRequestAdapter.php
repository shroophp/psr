<?php

namespace ShrooPHP\PSR\Requests;

use GuzzleHttp\Psr7\StreamWrapper;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UploadedFileInterface;
use ShrooPHP\Core\ImmutableArrayObject;
use ShrooPHP\Core\Request;
use ShrooPHP\Core\Session as ISession;
use ShrooPHP\Framework\Sessions\Session;
use ShrooPHP\Framework\Tokens\EmptyToken;
use ShrooPHP\Framework\Uploads\Upload;
use ShrooPHP\PSR\Requests\ServerRequestAdapter\Converter;
use ShrooPHP\PSR\Requests\ServerRequestAdapter\Converters\ShiftConverter;

/**
 * An adapter that allows PSR-compliant server requests to be compatible with
 * applications.
 */
class ServerRequestAdapter implements Request
{
	/**
	 * The server request being adapted.
	 *
	 * @var \Psr\Http\Message\ServerRequestInterface
	 */
	private $request;

	/**
	 * The session being associated with the server request.
	 *
	 * @var \ShrooPHP\Core\Session
	 */
	private $session;

	/**
	 * The converter being used to convert headers.
	 *
	 * @var \ShrooPHP\PSR\Requests\ServerRequestAdapter\Converter
	 */
	private $converter;

	/**
	 * The token being associated with the server request (or NULL if it is yet
	 * to be instantiated).
	 *
	 * @var \ShrooPHP\Framework\Tokens\EmptyToken|null
	 */
	private $token;

	/**
	 * Constructs an adapter for the given request.
	 *
	 * @param \Psr\Http\Message\ServerRequestInterface $request the server
	 * request to adapt
	 * @param \ShrooPHP\Core\Session|null $session the session to associate
	 * with the server request (or NULL to use the default)
	 * @param \ShrooPHP\PSR\Requests\ServerRequestAdapter\Converter $converter
	 * the converter to use to convert headers (or NULL to use the default)
	 */
	public function __construct(
			ServerRequestInterface $request,
			ISession $session = null,
			Converter $converter = null
	) {

		if (is_null($session)) {
			$session = new Session;
		}

		if (is_null($converter)) {
			$converter = new ShiftConverter;
		}

		$this->request = $request;
		$this->session = $session;
		$this->converter = $converter;
	}

	public function method()
	{
		return $this->request->getMethod();
	}

	public function path()
	{
		return $this->request->getUri()->getPath();
	}

	public function params()
	{
		return new ImmutableArrayObject($this->request->getQueryParams());
	}

	public function headers()
	{
		$converted = $this->converter->convert($this->request->getHeaders());

		return new ImmutableArrayObject($converted);
	}

	public function cookies()
	{
		return new ImmutableArrayObject($this->request->getCookieParams());
	}

	public function session()
	{
		return $this->session;
	}

	public function open()
	{
		return StreamWrapper::getResource($this->request->getBody());
	}

	public function data()
	{
		$data = $this->request->getParsedBody();

		if (!is_array($data)) {
			$data = array();
		}

		return new ImmutableArrayObject($data);
	}

	public function uploads()
	{
		return $this->toUploads($this->request->getUploadedFiles());
	}

	public function args()
	{
		return new ImmutableArrayObject($this->request->getAttributes());
	}

	public function token()
	{
		if (is_null($this->token)) {
			$this->token = new EmptyToken;
		}

		return $this->token;
	}

	public function root()
	{
		return $this;
	}

	/**
	 * Converts the given uploaded files to uploads.
	 *
	 * @param array $uploads the uploaded files to convert
	 * @param scalar|null $prefix the prefix to associate with each key (if any)
	 * @return array the converted uploaded files
	 */
	private function toUploads(array $uploads, $prefix = null)
	{
		$converted = array();

		foreach ($uploads as $key => $upload) {

			$prefixed = $key;

			if (!is_null($prefix)) {
				$prefixed = "{$prefix}[{$prefixed}]";
			}

			if (is_array($upload)) {
				$children = $this->toUploads($upload, $prefixed);
				$converted = array_merge($converted, $children);
			} elseif ($upload instanceof UploadedFileInterface) {
				$converted[$prefixed] = $this->toUpload($upload);
			}
		}

		return $converted;
	}

	/**
	 * Converts the given uploaded file to an upload.
	 *
	 * @param \Psr\Http\Message\UploadedFileInterface $upload the uploaded file
	 * to convert
	 * @return \ShrooPHP\Core\Upload the converted uploaded file
	 */
	private function toUpload(UploadedFileInterface $upload)
	{
		$stream = $upload->getStream();
		$converted = new Upload(
				$upload->getClientFilename(),
				$upload->getClientMediaType(),
				$upload->getSize(),
				"{$stream->getMetadata('uri')}",
				$upload->getError()
		);
		$stream->close();

		return $converted;
	}
}
