<?php

namespace ShrooPHP\PSR\Requests\ServerRequestAdapter;

/**
 * A converter of PSR-compliant headers.
 */
interface Converter
{
	/**
	 * Converts the given headers to an array.
	 *
	 * @param array $headers the headers to covert
	 * @return array the converted headers
	 * @see \Psr\Http\Message\MessageInterface::getHeaders()
	 */
	public function convert(array $headers);
}
