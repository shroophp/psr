<?php

namespace ShrooPHP\PSR\Requests\ServerRequestAdapter\Converters;

use ShrooPHP\PSR\Requests\ServerRequestAdapter\Converter;

/**
 * A convert of PSR-compliant headers that shifts the first value before
 * associating it with the original key.
 */
class ShiftConverter implements Converter
{
	public function convert(array $headers)
	{
		$converted = array();

		foreach ($headers as $header => $values) {

			foreach ($values as $value) {
				$converted[$header] = $value;
				break; // Retrieve only the first value.
			}
		}

		return $converted;
	}
}
