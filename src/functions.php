<?php

namespace ShrooPHP\PSR;

use GuzzleHttp\Psr7\ServerRequest;
use GuzzleHttp\Psr7\UploadedFile;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UploadedFileInterface;
use ShrooPHP\Core\Request;
use ShrooPHP\Core\Upload;

/**
 * Converts the given upload to an uploaded file.
 *
 * @param \ShrooPHP\Core\Upload $upload The upload to convert.
 * @return \Psr\Http\Message\UploadedFileInterface The converted upload.
 */
function to_uploaded_file(Upload $upload): UploadedFileInterface
{
	return new UploadedFile(
		$upload->path(),
		$upload->size(),
		$upload->error(),
		$upload->name(),
		$upload->type()
	);
}

/**
 * Converts the given uploads to uploaded files.
 *
 * @param \Traversable|array $uploads The uploads to convert.
 * @return array The converted uploads.
 */
function to_uploaded_files($uploads): array
{
	$converted = [];

	foreach ($uploads as $key => $upload) {

		$converted[$key] = to_uploaded_file($upload);
	}

	return $converted;
}

/**
 * Converts the given request to a server request.
 *
 * @param \ShrooPHP\Core\Request $request The request to convert.
 * @return \Psr\Http\Message\ServerRequestInterface The converted request.
 */
function to_server_request(Request $request): ServerRequestInterface
{
	return (new ServerRequest(
			$request->method(),
			$request->path(),
			$request->headers()->getArrayCopy(),
			$request->open()
	))->withCookieParams($request->cookies()->getArrayCopy())
		->withParsedBody($request->data()->getArrayCopy())
		->withQueryParams($request->params()->getArrayCopy())
		->withUploadedFiles(to_uploaded_files($request->uploads()));
}
