<?php

namespace ShrooPHP\PSR\Tests;

use ShrooPHP\Core\ImmutableArrayObject;
use ShrooPHP\Core\Openable;
use ShrooPHP\Core\Upload as IUpload;
use ShrooPHP\Framework\Requests\Request;
use ShrooPHP\Framework\Uploads\Upload;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\UploadedFileInterface;
use function ShrooPHP\PSR\to_server_request;
use function ShrooPHP\PSR\to_uploaded_file;
use function ShrooPHP\PSR\to_uploaded_files;

class FunctionsTest extends TestCase implements Openable
{
	public function open()
	{
		return fopen(__FILE__, 'rb') ?: null;
	}

	public function testToUploadedFile()
	{
		$expected = new Upload('name', 'type', 0b1010, __FILE__, UPLOAD_ERR_OK);

		$actual = to_uploaded_file($expected);

		$this->assertUploadedFile($expected, $actual);
	}

	public function testToUploadedFiles()
	{
		$expected = $this->toUploads();

		$actual = to_uploaded_files($expected);

		$this->assertUploadedFiles($expected, $actual);
	}

	public function testToServerRequest()
	{
		$params = ['q' => 'test'];
		$headers = ['X-Powered-By' => __FILE__];
		$cookies = ['cookie' => 'Oat & Raisin'];
		$data = ['password' => 'password'];
		$uploads = $this->toUploads();
		$contents = file_get_contents(__FILE__);

		$expected = new Request(
			'METHOD',
			'/path',
			new ImmutableArrayObject($params),
			new ImmutableArrayObject($headers),
			new ImmutableArrayObject($cookies),
			null,
			$this,
			new ImmutableArrayObject($data),
			$uploads
		);

		$actual = to_server_request($expected);

		foreach ($headers as $header => $value) {
			$headers[$header] = [$value];
		}

		$this->assertEquals($expected->method(), $actual->getMethod());
		$this->assertEquals($expected->path(), (string) $actual->getUri());
		$this->assertEquals($params, $actual->getQueryParams());
		$this->assertEquals($headers, $actual->getHeaders());
		$this->assertEquals($cookies, $actual->getCookieParams());
		$this->assertEquals($contents, $actual->getBody()->getContents());
		$this->assertEquals($data, $actual->getParsedBody());
		$this->assertUploadedFiles($uploads, $actual->getUploadedFiles());
	}

	private function toUploads(): array
	{
		return [
			'uploads[0]' => $this->toUpload('first_name', 'first_type', 1),
			'uploads[1]' => $this->toUpload('second_name', 'second_type', 2),
		];
	}

	private function toUpload(string $name, string $type, int $size): IUpload
	{
		return new Upload($name, $type, $size, __FILE__, UPLOAD_ERR_OK);
	}

	private function assertUploadedFile(
		IUpload $expected,
		UploadedFileInterface $actual
	) {
		$metadata = $actual->getStream()->getMetadata();
		$path = $metadata['uri'];

		$this->assertEquals($expected->name(), $actual->getClientFilename());
		$this->assertEquals($expected->error(), $actual->getError());
		$this->assertEquals($expected->path(), $path);
		$this->assertEquals($expected->size(), $actual->getSize());
		$this->assertEquals($expected->type(), $actual->getClientMediaType());
	}


	private function assertUploadedFiles(array $expected, array $actual)
	{
		foreach ($expected as $key => $upload) {
			$this->assertArrayHasKey($key, $actual);
			$this->assertUploadedFile($upload, $actual[$key]);
			unset($actual[$key]);
		}

		$this->assertEquals([], $actual);
	}
}
