<?php

namespace ShrooPHP\PSR\Http\Message;

use GuzzleHttp\Psr7\LazyOpenStream;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use ShrooPHP\Framework\Request\Responses\Response;
use ShrooPHP\PSR\Http\Message\ResponseAdapter;
use ShrooPHP\Core\Bufferers\Bufferer;

/**
 * A test case for \ShrooPHP\PSR\Http\Message\ResponseAdapter.
 */
class ResponseAdapterTest extends TestCase
{
	const CONTENT = 'Hello, world!';

	const CONTENT_TYPE = 'text/plain';

	public function testConstructor()
	{
		$this->assertResponseAdapter();
	}

	public function testConstructorWithCustomization()
	{
		$root = vfsStream::setup();
		$this->assertResponseAdapter(
				Response::HTTP_I_AM_A_TEAPOT,
				'42.0',
				'I am unable to brew coffee.',
				"{$root->url()}/buffer"
		);
	}

	private function toHeaders()
	{
		return array(
			'Cookie' => array(
				'Milk Chocolate',
				'Oat & Raisin',
			),
			'X-Powered-By' => array(
				'Delicious Cookies.',
			)
		);
	}

	private function assertResponseAdapter(
			$status = null,
			$version = null,
			$reason = null,
			$buffer = null
	) {

		$contents = self::CONTENT;
		$headers = $this->toHeaders();

		$response = Response::string($contents, self::CONTENT_TYPE);

		foreach ($headers as $header => $values) {
			foreach ($values as $value) {
				$response->addHeader($header, $value);
			}
		}

		$headers['Content-Type'] = array(self::CONTENT_TYPE);

		$adapter = new ResponseAdapter(
				$response->setCode($status),
				$version,
				$reason,
				is_null($buffer) ? null : new Bufferer($buffer)
		);

		if (is_null($status)) {
			$status = ResponseAdapter::STATUS;
		}

		if (is_null($version)) {
			$version = ResponseAdapter::VERSION;
		}

		if (!is_null($reason)) {
			$this->assertEquals($reason, $adapter->getReasonPhrase());
		}

		$this->assertEquals($contents, $adapter->getBody()->getContents());
		$this->assertEquals($version, $adapter->getProtocolVersion());
		$this->assertEquals($status, $adapter->getStatusCode());

		foreach ($headers as $header => $values) {

			$line = implode(', ', $values);
			$this->assertEquals($values, $adapter->getHeader($header));
			$this->assertEquals($line, $adapter->getHeaderLine($header));
			$this->assertTrue($adapter->hasHeader($header));
		}

		$this->assertFalse($adapter->hasHeader('Undefined'));

		$hugs = $adapter->withHeader('X-Powered-By', 'Hugs');
		$this->assertEquals(array('Hugs'), $hugs->getHeader('X-Powered-By'));
		$this->assertEquals('Hugs', $hugs->getHeaderLine('X-Powered-By'));

		$header = 'Cookie';
		$cookie = 'Double Chocolate';
		$added = $adapter->withAddedHeader($header, $cookie);
		$cookies = $headers[$header];
		$cookies[] = $cookie;
		$imploded = implode(', ', $cookies);
		$this->assertEquals($cookies, $added->getHeader($header));
		$this->assertEquals($imploded, $added->getHeaderLine($header));

		$removed = $adapter->withoutHeader('X-Powered-By');
		$subset = array(
			'Cookie' => $headers['Cookie'],
			'Content-Type' => $headers['Content-Type'],
		);
		$this->assertEquals($subset, $removed->getHeaders());

		$rebodied = $adapter->withBody(new LazyOpenStream(__FILE__, 'rb'));
		$body = $rebodied->getBody();
		$this->assertEquals(file_get_contents(__FILE__), $body->getContents());

		$patch = "{$version}.1";
		$patched = $adapter->withProtocolVersion($patch);
		$this->assertEquals($patch, $patched->getProtocolVersion());

		$restatus = $status + 1;
		$rephrase = "{$reason}!";
		$restated = $adapter->withStatus($restatus, $rephrase);
		$this->assertEquals($restatus, $restated->getStatusCode());
		$this->assertEquals($rephrase, $restated->getReasonPhrase());
	}
}
