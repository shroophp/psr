<?php

namespace ShrooPHP\PSR\Tests\Http\Message;

use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use ShrooPHP\PSR\Http\Message\ResponseTrait;

class ResponseTraitTest extends TestCase
{
	use ResponseTrait;

	public function testResponseTrait()
	{
		$this->assertSame($this->response()->getBody(), $this->getBody());
	}

	protected function response()
	{
		static $response = null;

		if (is_null($response)) {
			$response = new Response;
		}

		return $response;
	}
}

