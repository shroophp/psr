<?php

namespace ShrooPHP\PSR\Tests\Request\Response\Presenters;

use PHPUnit\Framework\TestCase;
use ShrooPHP\PSR\Request\Response\Presenters\BufferPresenter;
use ShrooPHP\Framework\Request\Responses\Response;

class BufferPresenterTest extends TestCase
{
	public function testBufferPresenter()
	{
		$response = new Response;
		$presenter = new BufferPresenter;

		$this->assertNull($presenter->response());

		$presenter->present($response);
		$this->assertSame($response, $presenter->response());

		$presenter->clear();
		$this->assertNull($presenter->response());
	}
}
