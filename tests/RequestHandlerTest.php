<?php

namespace ShrooPHP\PSR\Tests;

use GuzzleHttp\Psr7\Response as GuzzleHttpPsr7Response;
use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use ShrooPHP\Core\Bufferers\Bufferer;
use ShrooPHP\Core\Request as IRequest;
use ShrooPHP\Core\Request\Response as IResponse;
use ShrooPHP\Core\Request\Response\Presenter;
use ShrooPHP\Framework\Application;
use ShrooPHP\Framework\Application\Modifier;
use ShrooPHP\Framework\Request\Responses\Response;
use ShrooPHP\Framework\Requests\Request;
use ShrooPHP\PSR\RequestHandler;
use ShrooPHP\PSR\Request\Response\Presenters\BufferPresenter;
use org\bovigo\vfs\vfsStream;

class RequestHandlerTest extends TestCase implements Modifier
{
	const METHOD = 'EXTERMINATE';

	const URI = 'http://example.org/doctor';

	const STATUS = Response::HTTP_I_AM_A_TEAPOT;

	const CONTENT = 'Hello, world!';

	const CONTENT_TYPE = 'text/plain';

	public function modify(Application $app)
	{
		$app->method(self::METHOD, $app->_(function (IRequest $request) {

			return Response::string(self::CONTENT, self::CONTENT_TYPE)
					->setCode(self::STATUS);
		}));
	}

	public function testHandle()
	{
		$response = $this->toResponse();
		$handler = new RequestHandler($response);
		$this->assertHandle($handler, $response);
	}

	public function testHandleWithModifier()
	{
		$response = $this->toResponse();
		$handler = new RequestHandler($response);
		$this->assertHandle($handler, $response, $this);
	}

	public function testHandleWithModifierCallback()
	{
		$response = $this->toResponse();
		$handler = new RequestHandler($response);
		$this->assertHandle($handler, $response, array($this, 'modify'));
	}

	public function testRespond()
	{
		$this->assertRespond($this->toResponse());
	}

	public function testRespondWithModifier()
	{
		$response = new Response;
		$modifier = function (Application $app) use ($response) {

			$app->push(function () use ($response) {

				return $response;
			});
		};

		$this->assertRespond($this->toResponse(), $response, $modifier);
	}

	/**
	 * @runInSeparateProcess
	 */
	public function testServe()
	{
		$handler = new RequestHandler($this->toResponse());
		ob_start();
		$this->serve($handler);
		ob_end_clean();
		$this->assertTrue(true); // Mark the test as complete.
	}

	public function testServeWithPresenter()
	{
		$this->assertServe();
	}

	public function testServeWithPresenterAndModifier()
	{
		$headers = array(
			'Content-Type' => 'text/plain',
			'X-Powered-By' => 'Dilithium',
		);
		$response = new GuzzleHttpPsr7Response(Response::HTTP_I_AM_A_TEAPOT);

		foreach ($headers as $header => $value) {
			$response = $response->withHeader($header, $value);
		}

		$modifier = function (Application $app) use ($headers) {

			$app->push(function () use ($headers) {
				return new Response(Response::HTTP_I_AM_A_TEAPOT, $headers);
			});
		};
		$this->assertServe($response, $modifier);
	}

	public function testToServerRequest()
	{
		$extracted = null;
		$handled = false;
		$expected = $this->toRequest();
		$response = $this->toResponse();
		$handler = new RequestHandler($response);
		$modifier = function (Application $app) use (
				$handler,
				$expected,
				&$extracted
		) {

			$app->push(function (IRequest $request) use (
					$handler,
					$expected,
					&$extracted
			) {

				$unknown = new Request('', '');
				$actual = $handler->toServerRequest($request);
				$this->assertNull($handler->toServerRequest($unknown));
				$this->assertSame($expected, $actual);
				$extracted = $request;
				return null;
			});
		};

		$handler->apply($modifier);
		$this->assertSame($response, $handler->handle($expected));
		$this->assertNotNull($extracted);
		$this->assertNull($handler->toServerRequest($extracted));
	}

	private function toRequest()
	{
		return new ServerRequest(self::METHOD, self::URI);
	}

	private function toResponse()
	{
		$headers = array('Content-Type' => array('text/plain'));
		return new GuzzleHttpPsr7Response(404, $headers);
	}

	private function serve(
			RequestHandler $handler,
			Presenter $presenter = null
	) {

		$method = $_SERVER['REQUEST_METHOD'] ?? null;
		$uri = $_SERVER['REQUEST_URI'] ?? null;

		$_SERVER['REQUEST_METHOD'] = self::METHOD;
		$_SERVER['REQUEST_URI'] = self::URI;

		try {
			$handler->serve($presenter);
		} finally {
			$_SERVER['REQUEST_URI'] = $method;
			$_SERVER['REQUEST_URI'] = $uri;
		}
	}

	public function assertServe(
			ResponseInterface $expected = null,
			$modifier = null
	) {

		$presenter = new BufferPresenter;
		$default = $this->toResponse();
		$handler = new RequestHandler($default);

		if (is_null($expected)) {
			$expected = $default;
		}

		if (!is_null($modifier)) {
			$handler->apply($modifier);
		}

		$this->serve($handler, $presenter);

		$actual = $presenter->response();

		$this->assertNotNull($expected);
		$this->assertResponse($expected, $actual);

	}

	private function assertHandle(
			RequestHandler $handler,
			ResponseInterface $response,
			$modifier = null
	) {
		$request = $this->toRequest();
		$unknown = $request->withMethod(self::METHOD . '!');

		$this->assertSame($response, $handler->handle($request));

		if (!is_null($modifier)) {
			$handler->apply($modifier);
		}

		$this->assertSame($response, $handler->handle($unknown));

		$adapted = $handler->handle($request);

		if (is_null($modifier)) {

			$this->assertSame($response, $adapted);

		} else {

			$this->assertNotSame($response, $adapted);
			$this->assertEquals(self::STATUS, $adapted->getStatusCode());
			$this->assertEquals(self::CONTENT, $adapted->getBody()->getContents());

			$headers = array('Content-Type' => array(self::CONTENT_TYPE));
			$this->assertEquals($headers, $adapted->getHeaders());

		}

	}

	public function assertRespond(
			ResponseInterface $default,
			IResponse $expected = null,
			$modifier = null
	) {

		$request = new Request(self::METHOD, '/');
		$handler = new RequestHandler($default);

		if (!is_null($modifier)) {
			$handler->apply($modifier);
		}

		$actual = $handler->respond($request);

		if (is_null($expected)) {
			$this->assertResponse($default, $actual);
		} else {
			$this->assertSame($expected, $actual);
		}
	}

	private function assertResponse(
			ResponseInterface $expected,
			IResponse $actual
	) {

		$contents = '';
		$headers = array();

		$this->assertEquals($expected->getStatusCode(), $actual->code());

		foreach ($actual->headers() as $header => $value) {
			if (isset($headers[$header])) {
				$headers[$header][] = $value;
			} else {
				$headers[$header] = array($value);
			}
		}

		$this->assertEquals($expected->getHeaders(), $headers);

		$content = $actual->content();

		if (!is_null($content)) {
			$root = vfsStream::setup();
			$bufferer = new Bufferer("{$root->url()}/contents");
			$handle = $bufferer->buffer($content);
			$contents = stream_get_contents($handle);
			fclose($handle);
		}

		$this->assertEquals($expected->getBody()->getContents(), $contents);
	}
}
