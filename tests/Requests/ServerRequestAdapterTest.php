<?php

namespace ShrooPHP\PSR\Tests\Requests;

use GuzzleHttp\Psr7\LazyOpenStream;
use GuzzleHttp\Psr7\ServerRequest;
use GuzzleHttp\Psr7\UploadedFile;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use ShrooPHP\Core\Session as ISession;
use ShrooPHP\Core\Upload as IUpload;
use ShrooPHP\Framework\Sessions\Session;
use ShrooPHP\Framework\Tokens\EmptyToken;
use ShrooPHP\Framework\Uploads\Upload;
use ShrooPHP\PSR\Requests\ServerRequestAdapter;
use ShrooPHP\PSR\Requests\ServerRequestAdapter\Converter;
use ShrooPHP\PSR\Requests\ServerRequestAdapter\Converters\ShiftConverter;

class ServerRequestAdapterTest extends TestCase
{
	const METHOD = 'EXTERMINATE';

	const URI = 'http://example.org/path/to/page';

	public function testConstructor()
	{
		$headers = $this->toHeaders();
		$request = $this->toServerRequest($headers, $this->toUploadedFiles());

		$this->assertServerRequestAdapter(
				$request,
				$headers,
				$this->toUploads()
			);
	}

	private function toServerRequest(array $headers, array $uploads)
	{
		return (new ServerRequest(
				self::METHOD,
				self::URI,
				$headers,
				new LazyOpenStream(__FILE__, 'rb')
			))->withUploadedFiles($uploads);
	}

	private function toHeaders()
	{
		return array(
			'X-Powered-By' => array(
				"The Hitchhiker's Guide To The Galaxy",
			),
			"Don't" => array(
				'Panic',
				'Listen to Vogon Poetry'
			),
		);
	}

	private function toUploadedFiles()
	{
		return array(
			'upload' => new UploadedFile(
					__FILE__,
					1,
					UPLOAD_ERR_OK,
					basename(__FILE__),
					'text/plain'
				),
			'nested' => array(
				'upload' => new UploadedFile(
						__FILE__,
						2,
						UPLOAD_ERR_OK,
						basename(__FILE__),
						'text/plain'
					),
			),
		);
	}

	private function toUploads()
	{
		return array(
			'upload' => new Upload(
					basename(__FILE__),
					'text/plain',
					1,
					__FILE__,
					UPLOAD_ERR_OK
				),
			'nested[upload]' => new Upload(
					basename(__FILE__),
					'text/plain',
					2,
					__FILE__,
					UPLOAD_ERR_OK
				),
		);
	}

	private function assertServerRequestAdapter(
			ServerRequestInterface $request,
			array $headers,
			array $uploads,
			ISession $session = null,
			Converter $converter = null
	) {

		$matches = array();
		$adapter = new ServerRequestAdapter($request, $session, $converter);

		if (is_null($converter)) {
			$converter = new ShiftConverter;
		}

		preg_match('|^https?://(.*?)[/$]|', self::URI, $matches);
		$pushed = $converter->convert($headers);
		$pushed['Host'] = $matches[1];
		$this->assertEquals($pushed, $adapter->headers()->getArrayCopy());

		if (is_null($session)) {
			$this->assertInstanceOf(Session::class, $adapter->session());
		} else {
			$this->assertSame($session, $adapter->session());
		}

		$attributes = $request->getAttributes();
		$this->assertEquals($attributes, $adapter->args()->getArrayCopy());

		$cookies = $request->getCookieParams();
		$this->assertEquals($cookies, $adapter->cookies()->getArrayCopy());

		$body = $request->getParsedBody();

		if (!is_array($body)) {
			$body = array();
		}

		$this->assertEquals($body, $adapter->data()->getArrayCopy());

		$this->assertEquals($request->getMethod(), $adapter->method());

		$handle = $adapter->open();
		$contents = $request->getBody()->getContents();
		$this->assertEquals(0, fseek($handle, 0));
		$this->assertNotEquals('', $contents);
		$this->assertEquals($contents, stream_get_contents($handle));
		$this->assertTrue(fclose($handle));

		$params = $request->getQueryParams();
		$this->assertEquals($params, $adapter->params()->getArrayCopy());

		$this->assertEquals($request->getUri()->getPath(), $adapter->path());

		$this->assertInstanceOf(EmptyToken::class, $adapter->token());

		$this->assertUploads($uploads, $adapter->uploads());

		$this->assertSame($adapter, $adapter->root());
	}

	private function assertUploads(array $expected, array $actual)
	{
		foreach ($expected as $key => $upload) {

			$this->assertArrayHasKey($key, $actual);
			$this->assertInstanceOf(IUpload::class, $actual[$key]);

			$this->assertEquals($upload->name(), $actual[$key]->name());
			$this->assertEquals($upload->type(), $actual[$key]->type());
			$this->assertEquals($upload->size(), $actual[$key]->size());
			$this->assertEquals($upload->path(), $actual[$key]->path());
			$this->assertEquals($upload->error(), $actual[$key]->error());
		}
	}
}
